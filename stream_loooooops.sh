#!/bin/sh

set -e

# Do we have config file?
if [ ! -e stream_loooooops-config.xml ]
then
  echo ":( write an ezstream config file plz"
  exit 1
fi

# We keep trying to read the playlist
# it could take some time for the download script to
# generate one the first time
while true
do
  if [ -e playlist_loooooops.m3u ]
  then
    echo ":) playlist found"
    ezstream -v -c stream_loooooops-config.xml
    break
  else
    echo ":( playlist not found, trying again"
    sleep 2s
  fi
done
